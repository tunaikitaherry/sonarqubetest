package com.test.sonarqubetest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import javax.validation.constraints.AssertTrue;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.test.sonarqubetest.controller.HelloWorldController;

@RunWith(SpringRunner.class)
@WebMvcTest(value = HelloWorldController.class, secure = false)
public class HelloWorldControllerTest {
	
	@Autowired
	private MockMvc mockMvc;
	
	@Test
	public void helloWorldTest() throws Exception {
		RequestBuilder requestBuilder = MockMvcRequestBuilders.get(
				"/hello-world");

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		
		System.out.println(result.getResponse());
		String expected = "Hello World";

		assertEquals(expected,result.getResponse().getContentAsString());
		
	}
	
	@Test
	public void helloWorldBeanTest() throws Exception {
		RequestBuilder requestBuilder = MockMvcRequestBuilders.get(
				"/hello-world-bean").accept(
				MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		
		System.out.println(result.getResponse());
		String expected = "{\"message\":\"Hello World Bean\"}";

		
		JSONAssert.assertEquals(expected, result.getResponse()
				.getContentAsString(), false);
	}
	

}
